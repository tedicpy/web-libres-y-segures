// Variables

const profileCard = gsap.utils.toArray(".profile-card"); // Cada card individual

let mm = gsap.matchMedia(); // MatchMedia para solo aplicar la animacion cuando sea necesaria

mm.add("(min-width: 1300px)", () => {
  profileCard.forEach((el) => {
    const parrafo = el.querySelector(".profile-card-text"); // Cada parrafo
    const boton = el.querySelector(".profile-card-button"); // Cada boton
    const imagen = el.querySelector(".profile-card-image"); // Cada imagen

    let tlCards = gsap.timeline({
      paused: true,
      reversed: true,
      defaults: {
        ease: "power3.inOut",
      },
    });

    //   console.log(card);

    tlCards
      .from(parrafo, {
        autoAlpha: 0,
        x: "100px",
        y: "50px",
        duration: 0.5,
      })
      .from(
        boton,
        {
          autoAlpha: 0,
          x: "100px",
          y: "50px",
          duration: 0.5,
        },
        "-=0.3"
      )
      .to(
        imagen,
        {
          x: "180px",
          y: "-200px",
          duration: 0.5,
        },
        "-=0.2"
      );
    //   mm.add("(min-width: 1300px)", () => {
    el.addEventListener("mouseenter", () => {
      el.classList.add("active");
      tlCards.reversed() ? tlCards.play() : tlCards.reverse();
    });
    el.addEventListener("mouseleave", () => {
      el.classList.remove("active");
      tlCards.reversed() ? tlCards.play() : tlCards.reverse();
    });
    //   });
  });
});

// Just checking
