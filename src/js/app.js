const menuToggle = document.querySelector(".toggle-menu");
const containerMenu = document.querySelector(".nav__list");
const itemsMenuDesktop = document.querySelectorAll(
  ".nav__nivel-superior.con-submenu"
);

// Abrir / cerrar menu al hacer click en el toggle
menuToggle.addEventListener("click", function () {
  containerMenu.classList.toggle("menu-cerrado");
  menuToggle.classList.toggle("menu-cerrado");
});

// Ponerle la clase menu-abierto a cada menu item, y sacarla de los siblings cuando sea necesario
itemsMenuDesktop.forEach((itemMenu) => {
  itemMenu.addEventListener("click", () => {
    const isActive = itemMenu.classList.contains("menu-abierto");
    removeActiveClasses();

    if (!isActive) {
      itemMenu.classList.add("menu-abierto");
    }
  });
});

function removeActiveClasses() {
  itemsMenuDesktop.forEach((itemMenu) => {
    itemMenu.classList.remove("menu-abierto");
  });
}

// Ocultar el menu al hacer click fuera del area del mismo
document.addEventListener("click", (e) => {
  if (!containerMenu.contains(e.target)) {
    itemsMenuDesktop.forEach((item) => {
      item.classList.remove("menu-abierto");
    });
  }
});

// El slider

document.addEventListener("DOMContentLoaded", function () {
  var splide = new Splide(".splide", {
    type: "loop",
    perPage: 3,
    gap: "2em",
    padding: "2em",
    breakpoints: {
      768: {
        perPage: 2,
      },
      640: {
        perPage: 1,
      },
    },
  });
  splide.mount();
});

// Acordeon en el home

gsap.registerPlugin(Flip, ScrollTrigger);

const groups = gsap.utils.toArray(".accordion__section");

groups.forEach((el) => {
  el.addEventListener("click", () => toggleMenu(el));
});

function toggleMenu(el) {
  let state = Flip.getState(el.querySelector(".accordion__content"));
  el.classList.toggle("active");
  Flip.from(state);
}

// Pin Menu Lateral en Historia y Derechos

let st = ScrollTrigger.create({
  trigger: ".pin-trigger",
  pin: ".menu-flotante",
  pinSpacing: false,
  // start: "top center",
  // end: "+=500",
});

// Nuevo intento animacion cards en el home

// let mm = gsap.matchMedia(); // MatchMedia para solo aplicar la animacion cuando sea necesaria

// function initProfileAnimation() {
//   mm.add("(min-width: 1300px)", () => {
//     const profileCard = gsap.utils.toArray(".profile-card");

//     profileCard.forEach((el) => {
//       el.addEventListener("mouseenter", () => animarCard(el));
//       el.addEventListener("mouseleave", () => animarCard(el));
//     });

//     function animarCard(el) {
//       const state = Flip.getState(profileCard);
//       const cardImg = el.querySelector(".profile-card-right");
//       const imgState = Flip.getState(cardImg);

//       el.classList.toggle("active");

//       Flip.from(state, {
//         ease: "power4.inOut",
//         duration: 1,
//         absolute: true,
//       });
//       Flip.from(
//         imgState,
//         {
//           ease: "power4.inOut",
//           duration: 1,
//         },
//         "-=1"
//       );
//     }
//   });
// }

// window.addEventListener("load", function (event) {
//   initProfileAnimation();
// });
