module.exports = {
  es: {
    tituloPrincipal: "¿Por qué hacemos esta web?",
    parrafo1: `La vida en Internet es real y es un espacio para comunicarnos e informarnos. Para nuestra comunidad LGTBIQ+ a menudo puede ser un espacio de escape y salvavidas.`,
    parrafo3: `El acceso a la red es sumamente útil para la comunidad LGTBIQ+, tanto
        para informarnos sobre sexualidad, como para descubrir amigues, nuevas
        comunidades diversas, y acceder a contenidos para ampliar nuestros
        conocimientos. También es un espacio que ayuda a afirmar que no estamos
        solxs y que hay otras personas que piensan en las mismas cosas que nosotrxs.`,
    tituloSecundario:
      "Beneficios de habitar en Internet de forma libres y segures",
    listItem1: `Podemos conectar con comunidades LGTBQ+ alrededor del mundo.`,
    listItem2: `Nos permite explorar la identidad LGBTQ+, así como conocer derechos y garantías de protección.`,
    listItem3: `Nos ayuda a conocer y compartir experiencias con otras personas.`,
    listItem4: `Nos permite generar relaciones, citas, reuniones en línea, sea para jugar videojuegos, charlar, sextear u otra forma de compartir en Internet.`,
  },
  en: {
    tituloPrincipal: "Why do we make this site?",
    parrafo1: `Life on the Internet is real, and it is a space for us to communicate and stay informed. For our LGTBIQ+ community it can often be an escape and a lifeline.`,
    parrafo3: `Access to the web is extremely useful for the LGTBIQ+ community, both to inform ourselves about sexuality and to discover friends, new and diverse communities, and accessing content to broaden our knowledge. It is also a space that helps to affirm that we are not alone, and that there are others out there who think about the same things we do.`,
    tituloSecundario: "Benefits of living freely and safely on the Internet:",
    listItem1: `We can connect with LGTBQ+ communities around the world.`,
    listItem2: `It allows us to explore LGBTQ+ identity, as well as to learn about rights and protection guarantees.`,
    listItem3: `It helps us to know and share experiences with other people.`,
    listItem4: `It allows us to build relationships, dates, online meetings, whether for playing video games, chatting, sexting or any other form of sharing on the Internet.`,
  },
};
