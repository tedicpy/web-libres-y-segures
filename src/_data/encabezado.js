module.exports = {
  es: {
    seguridad: "Seguridad Digital",
    queer: {
      nombre: "Queer de Barrio",
      url: "/es/queer-de-barrio/",
    },
    activista: {
      nombre: "Activista TLGBIQ+",
      url: "/es/activista-TLGBIQ/",
    },
    figura: {
      nombre: "Figura pública",
      url: "/es/figura-publica/",
    },
    comunidad: "Comunidad TLGBIQ+",
    historia: {
      nombre: "Historia",
      url: "/es/historia/",
    },
    derechos: {
      nombre: "Derechos",
      url: "/es/derechos/",
    },
    // podcast: {
    //   nombre: "Podcast",
    //   url: "/es/podcast/",
    // },
    podcast: {
      nombre: "Podcast",
      url: "/es/podcast/",
    },
    curso: {
      nombre: "Curso",
      url: "/es/curso/",
    },
  },
  en: {
    seguridad: "Digital Safety",
    queer: {
      nombre: "Neighborhood Queer",
      url: "/en/neighborhood-queer/",
    },
    activista: {
      nombre: "TLGBIQ+ Activist",
      url: "/en/TLGBIQ-activist/",
    },
    figura: {
      nombre: "Public Figure",
      url: "/en/public-figure/",
    },
    comunidad: "TLGBIQ+ Community",
    historia: {
      nombre: "History",
      url: "/en/history/",
    },
    derechos: {
      nombre: "Rights",
      url: "/en/rights/",
    },
    // podcast: {
    //   nombre: "Podcast",
    //   url: "/en/podcast/",
    // },
    podcast: {
      nombre: "Podcast",
      url: "/en/podcast/",
    },
    curso: {
      nombre: "Course",
      url: "/en/course/",
    },
  },
};
