module.exports = {
  es: {
    titulito: "Curso de",
    titulo: "Seguridad Digital",
    texto:
      "Sumate a este curso auto-administrado para activistas y defensores de derechos humanos.",
    boton: "Aprender más",
  },
  en: {
    titulito: "Course",
    titulo: "Digital Safety",
    texto:
      "Join this self-paced course for activists and human rights defenders.",
    boton: "Learn more",
  },
};
