module.exports = {
  es: {
    tituloXs: "Conoce más sobre",
    titulo: "La conquista de los derechos TLGIBQ+ en Paraguay",
    tituloHistoria: "Historia",
    textoHistoria:
      "Un pequeño recorrido de la lucha por el reconocimiento de las personas TLGBIQ+",
    urlHistoria: "/es/historia",
    tituloDerechos: "Derechos",
    textoDerechos: "Insumos para proteger nuestra comunidad TLGBIQ+",
    urlDerechos: "/es/derechos",
    boton: "Leer más",
  },
  en: {
    tituloXs: "Learn more about",
    titulo:
      "The history of achieving the rights of the TLGBIQ+ community in Paraguay",
    tituloHistoria: "History",
    textoHistoria:
      "A short overview of the struggle for the recognition of TLGBIQ+ people.",
    urlHistoria: "/en/history",
    tituloDerechos: "Rights",
    textoDerechos: "Tools to protect our TLGBIQ+ community",
    urlDerechos: "/en/rights",
    boton: "Learn more",
  },
};
