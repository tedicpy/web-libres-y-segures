module.exports = {
  es: {
    titulo: `Escucha el Podcast "Libres y segures en Internet"`,
    url: "/es/podcast",
  },
  en: {
    titulo: `Listen to the Podcast "Libres y segures en Internet"`,
    url: "/en/podcast",
  },
};
