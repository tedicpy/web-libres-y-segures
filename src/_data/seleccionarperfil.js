module.exports = {
  es: {
    titulo: "SELECCIONA TU PERFIL DE",
    tituloInterno: "Explorá más perfiles de",
    tituloXl: "SEGURIDAD DIGITAL",
    textoBoton: "Leer más",
    tituloQueer: "Queer de barrio",
    urlQueer: "/es/queer-de-barrio",
    textoQueer: `¿A qué tipo de problemas de seguridad digital se enfrentan las personas TLGBIQ+ en el día a día?`,
    tituloActivista: "Activista TLGBIQ+",
    urlActivista: "/es/activista-TLGBIQ",
    textoActivista:
      "¿Cómo debemos cuidarnos si utilizamos la tecnología en nuestra labor como activistas TLGBIQ+?",
    tituloFigura: "Figura pública",
    urlFigura: "/es/figura-publica",
    textoFigura: `¿A qué riesgos de seguridad digital se exponen las personas TLGBIQ+ de alta visibilidad pública?`,
  },
  en: {
    titulo: "SELECT YOUR",
    tituloInterno: "Explore another",
    tituloXl: "DIGITAL SAFETY PROFILE",
    tituloQueer: "Neighborhood Queer",
    urlQueer: `/en/neighborhood-queer`,
    textoQueer: `What kind of digital security issues do TLGBIQ+ people face on a daily basis?`,
    textoBoton: "Read more",
    tituloActivista: "TLGBIQ Activist",
    urlActivista: `/en/TLGBIQ-activist`,
    textoActivista:
      "How should we take care of ourselves if we use technology in our work as TLGBIQ+ activists?",
    tituloFigura: "Public Figure",
    urlFigura: `/en/public-figure`,
    textoFigura: `What digital safety risks are TLGBIQ individuals with high public visibility exposed to?`,
  },
};
