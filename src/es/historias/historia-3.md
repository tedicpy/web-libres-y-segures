---
numeroHistoria: Historia 3
titulo: Mercedes fue estafada
Adversario: Estafadores
imagen: "/imagenes/historias/historia-bot-ilustracion.png"
tags:
  - queer
  - queer_espanol
---

Mercedes salió del closet hace poco, y está lista para empezar a salir con chicas. Aunque tuvo la oportunidad de ir un par de veces a espacios queer a conocer a otras mujeres con sus mismos intereses, se siente más cómoda utilizando aplicaciones de citas, especialmente por la situación de pandemia. En una de esas conoció a Fabiana, y rápidamente se hicieron muy cercanas. Aunque a Fabiana no le gustaba compartir fotos personales, hacer llamadas ni enviar audios, se escribían todos los días, y desarrollaron una relación muy íntima.

Un día, Fabiana escribe a Mercedes muy afectada, y le comenta que su madre había entrado a UTI el día anterior. Estaba con un cuadro muy serio de COVID-19, y en su familia estaban desesperados, ya que no contaban con recursos suficientes para cubrir los gastos médicos asociados. Conmovida por la situación de Fabiana, Mercedes le ofrece ayudar, enviando una suma de dinero importante a través de Giros Tigo en forma de préstamo. Una vez realizada esta transacción, Mercedes nota que fue bloqueada por Fabiana tanto en WhatsApp como en la aplicación de citas donde se conocieron. Luego de buscar comunicarse con Fabiana a través de distintos medios, acepta finalmente que fue víctima de una estafa.

<div class="apartado">

## ¿Cómo pudo haber evitado Mercedes esta situación?

El informe Rohendu 2020 realizado por Aireana, grupo por la defensa de los derechos de las lesbianas en Paraguay, expone que muchos jóvenes TLGBIQ+ denunciaron a la línea de ayuda Rohendu ser víctimas de robo, estafas o incluso agresiones por parte de usuarios de redes sociales y aplicaciones de citas.

El caso de Mercedes es un ejemplo de catfishing, un tipo de estafa donde el victimario crea un perfil falso y lo utiliza para generar una relación cercana con una víctima, con el objetivo de estafar o abusar de una persona en concreto. Esta actividad puede usarse para obtener beneficios económicos, comprometer a la víctima de alguna manera o para generar angustia. El catfishing es un tipo de phishing, un término que engloba distintas actividades en línea que tienen como objetivo extraer información o estafar a una víctima.

</div>
