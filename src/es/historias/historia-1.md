---
numeroHistoria: Historia 1
titulo: Carlos vs. las redes sociales
adversario: Vecinos chismosos
imagen: "/imagenes/historias/historia-top-ilustracion.png"
tags:
  - queer
  - queer_espanol
---

Carlos es una persona muy amigable, y le gusta coleccionar amigos en redes sociales. Es muy querido y conocido por todas las personas de su trabajo, universidad y barrio. A Carlos también le gusta mucho salir a bailar. Un amigo suyo subió fotografías de ellos en la última fiesta drag de Hollywood, la famosa disco gay de Asunción, y lo etiquetó en Facebook. Aunque Carlos se aseguró de esconder los posteos de su feed a sus familiares (porque todavía no sabían sobre su orientación sexual), una vecina suya vio las fotos y no pasó mucho tiempo hasta que fue el chisme en la peluquería del barrio. Su familia se enteró y ahora Carlos está durmiendo en casa de un amigo para evitar confrontaciones.

<div class="apartado">

## ¿Cómo pudo haber evitado Carlos esta situación?

Es importante que cada une de nosotres identifique qué información prefiere que se encuentre en un perfil público y en un perfil privado. Las opciones de privacidad en facebook nos permiten tomar control acerca de quién ve qué tipo de información en nuestros perfiles públicos, además, nos permiten controlar quién puede asociar información a nuestra identidad (por ejemplo, elegir si queremos ser etiquetados en fotos o no).

</div>
