---
numeroHistoria: Historia 2
titulo: Sofía y sus contraseñas
Adversario: Compañero de trabajo transfóbico
imagen: "/imagenes/historias/historia-mid-ilustracion.png"
tags:
  - queer
  - queer_espanol
---

Sofía trabaja en el call center de una empresa de telefonía local desde hace ya más de un año. Aunque no tiene problemas en su trabajo, piensa mucho en cómo logrará conseguir uno donde pueda ser ella misma. Como chica trans en un ambiente conservador, siente mucho miedo de mostrarse como realmente es, y debe vestirse y actuar como hombre para ir a trabajar.

Al volver de su hora de almuerzo un día, nota que su teléfono móvil, que creía haber dejado en su mochila, se encuentra ahora sobre su mesa de trabajo. Sin darle mucha importancia, guarda su teléfono y continúa con sus actividades laborales. Un par de horas después, revisa sus mensajes y ve que alguien envió desde su propio teléfono varias fotografías de ella al grupo de WhatsApp laboral, revelando así su identidad como mujer trans. Es ahí donde se da cuenta que algún compañero de trabajo revisó su teléfono móvil, encontró sus fotografías personales, y decidió exponerlas. El malestar que le provocó esta agresión no le permitió volver a la oficina al día siguiente, y teme perder su sustento a causa de esta situación.

<div class="apartado">

## ¿Cómo pudo haber evitado Sofía esta situación?

Nuestros teléfonos, computadoras, correos y cuentas en línea guardan un montón de información nuestra. La exposición de esta información por parte de personas con malas intenciones puede vulnerar nuestro bienestar. Es importante proteger esta información, y uno de los aspectos más importantes para protegerla es el uso adecuado de contraseñas. Bloqueá el acceso a tus dispositivos móviles a través del bloqueo de pantalla con contraseña, y utilizá en tus cuentas en línea contraseñas largas, únicas para cada cuenta, con números, letras y caracteres y cambialas periódicamente. Te recomendamos además utilizar un gestor de contraseñas como Bitwarden para almacenarlas y no olvidarlas.

</div>
