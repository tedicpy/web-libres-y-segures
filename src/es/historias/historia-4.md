---
numeroHistoria: Historia 4
titulo: Isaura contra la policía
imagen: "/imagenes/historias/historia-top-ilustracion.png"
tags:
  - activistas
  - activistas_espanol
---

Isaura es activista feminista y TLGBIQ+ viviendo en Hernandarias. En la última marcha del orgullo realizada en su localidad, ella y un grupo de amigues presenció cómo la policía maltrataba a un grupo de mujeres trans. Isaura utilizó su teléfono móvil para filmar la agresión, pero un oficial la vio y rápidamente le arrebató el teléfono, que estaba protegido a través de huella dactilar.

A través del uso de la fuerza, el oficial consiguió que Isaura colocara su dedo en el lector de huella, pudiendo así acceder al dispositivo y eliminar la evidencia. Aunque Isaura intentó denunciar la agresión, no fue escuchada a falta de pruebas.

<div class="apartado">

## ¿Qué medidas pudo haber tomado Isaura para proteger su dispositivo?

Es importante tener en cuenta que cuando participamos de marchas, manifestaciones o protestas, debemos utilizar estrategias que nos ayuden a cuidar tanto nuestro cuerpo físico como el digital. El [Toolkit de seguridad digital para acciones colectivas](https://www.tedic.org/wp-content/uploads/2020/01/Toolkit-TEDIC_Formato-Digital.pdf) de TEDIC cuenta con una serie de herramientas útiles para implementar antes, durante y después de participar en una manifestación.

En el caso particular de lo ocurrido con Isaura, el uso de bloqueo de dispositivos a través de datos biométricos, como el rostro o el dedo, puede traer consigo varios problemas de seguridad. La huella dactilar es relativamente fácil de conseguir, ya sea a través de la fuerza u otros medios (por ejemplo, mientras el dueño del teléfono duerme).

</div>
