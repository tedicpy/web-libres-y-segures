---
numeroHistoria: Historia 5
titulo: Protección de espacios colectivos en línea
tags:
  - activistas
  - activistas_espanol
---

Noah es activista TLGBIQ+ de la colectiva Distintxs, y maneja las redes sociales del grupo de activistas de su comunidad de manera voluntaria. Noah es une chique no binarie que convive con su familia. Aunque se lleva bien con la mayoría de los miembros de su hogar, tiene una relación muy accidentada con su hermano mayor, que no acepta su identidad de género.

Una mañana, Noah despierta con su teléfono rebosando de mensajes de compañeres del grupo de activistas. La página de facebook del grupo había sido hackeada, ya ninguno de ellos tenía acceso, y prácticamente todo el contenido que habían publicado a lo largo de varios años había sido eliminado. Luego de varios intentos de recuperar la página original, terminaron por darse por vencides, y empezaron una página nueva, sin comprender nunca qué fue lo que ocurrió.

Meses después, durante una pelea familiar, el hermano de Noah confiesa haber ingresado a su computadora personal, donde encontró abierto su perfil de Facebook y correo electrónico, y utilizó los permisos de administrador de Noah y su correo para tomar control de la página y eliminar su contenido.

<div class="apartado">

## ¿Cómo pudo la colectiva haber evitado esta situación?

Es importante destacar que, aunque el ataque vino por parte del hermano de Noah, no es culpa de elle, ya que el cuidado de la página es una responsabilidad colectiva. La implementación de una política adecuada de contraseñas para Distintxs es fundamental para la protección de los bienes de la organización. Es además importante que los miembros generen espacios de debate donde se identifiquen cuáles podrían ser posibles amenazas a su labor de activismo, cuáles serían las posibles consecuencias de un ataque, y cómo prevenir futuros incidentes.

</div>
