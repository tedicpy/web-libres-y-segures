---
numeroHistoria: Historia 7
titulo: Liliana y Jorge son extorsionados
imagen: "/imagenes/historias/historia-top-ilustracion.png"
tags:
  - figuras
  - figuras_espanol
---

Liliana es una mujer muy reconocida en el ámbito político nacional. A causa del ambiente conservador de su país, hace años tomó la decisión de ocultar su identidad sexual como lesbiana. Liliana está además casada con Jorge, famoso conductor de televisión que también se encuentra en el closet. Tanto Jorge como Liliana utilizan aplicaciones de citas para vivir su vida sexual de forma plena. En una de esas, ella y Jorge reciben un correo con fotografías íntimas de él y conversaciones sexuales en línea de tinte homoerótico, y la amenaza de publicar la actividad de Jorge, lo cual podría arruinar la carrera de ambos. A cambio del silencio, el extorsionador pedía una gran suma de dinero.

<div class="apartado">

## ¿Cómo podían haber evitado esta situación Liliana y Jorge?

El sexting no entraña riesgos por sí mismo y es parte de nuestros derechos sexuales y reproductivos, pero en la práctica puede llegar a ser problemático, dando lugar a muchas situaciones complicadas: violación de nuestros derechos a la intimidad o a la libertad de expresión, violación de nuestros derechos sexuales a través de la extorsión o distribución de imágenes íntimas sin consentimiento, así como la publicación de datos personales sin autorización.

Desde TEDIC recomendamos utilizar aplicaciones que protejan nuestra privacidad cuando realizamos sexting. Signal es un mensajero que cuenta con privacidad por diseño, es decir, fue creado pensando en la privacidad del usuario desde el inicio. Funcionalidades como borrado periódico de textos, borroneado de rostros, y cifrado punto a punto hacen a esta herramienta un aliado a la hora de expresar nuestro deseo sexual utilizando medios digitales.

</div>
