---
numeroHistoria: Historia 6
titulo: Zoombombing en pandemia
imagen: "/imagenes/historias/historia-bot-ilustracion.png"
tags:
  - activistas
  - activistas_espanol
---

José es parte de una organización que trabaja por la defensa de los derechos de la comunidad TLGBIQ+ desde hace ya varios años. Su organización realiza encuentros, charlas y debates acerca de las problemáticas de la comunidad. Aunque estos encuentros siempre fueron presenciales, la situación de pandemia obligó a empezar a generar eventos virtuales, buscando así mantener los nexos comunales en épocas de aislamiento físico.

Hasta ahora, los encuentros virtuales no han tenido buenos resultados. El primer encuentro organizado por José fue escenario de un caso de zoombombing, donde personas antiderechos extrañas entraron a la charla a irrumpir e insultar a los participantes. Estas situaciones, que se repiten de vez en cuando, han desanimado bastante a Jóse y a su comunidad.

<div class="apartado">

## ¿Cómo puede José ayudar a mejorar esta situación?

El uso de herramientas de comunicación y videoconferencias adecuadas es fundamental para evitar incidentes de este estilo. Utilizar herramientas libres como Jitsi para realizar estos encuentros, y enviar los enlaces de acceso a través de medios seguros de comunicación, como Signal, puede ayudar a evitar la intrusión de personas no deseadas a este tipo de encuentros.

</div>
