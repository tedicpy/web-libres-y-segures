---
numeroHistoria: Story 2
titulo: Sofia and her passwords
Adversary: transphobic co-worker
imagen: "/imagenes/historias/historia-mid-ilustracion.png"
tags:
  - queer_ingles
---

Sofia has been working in the call center of a local phone company for more than a year now. Although she has no problems in her job, she thinks a lot about how she will be able to get a job where she can be herself. As a trans girl in a conservative environment, she is very afraid to show herself as she really is and has to dress and act like a man to go to work.

Coming back from her lunch break one day, she notices that her cell phone, which she thought she had left in her backpack, is now on her desk at work. Without giving it much thought, she puts her phone away and continues with her work activities. A couple of hours later, she checks her messages and sees that someone sent from her own phone several pictures of her to the work WhatsApp group, thus revealing her identity as a trans woman. That's when she realizes that a co-worker checked her cell phone, found her personal photographs, and decided to expose them. The discomfort caused by this aggression did not allow her to return to the office the next day, and she is afraid of losing her livelihood because of this situation.

<div class="apartado">

## How could Sophia have avoided this situation?

Our phones, computers, emails and online accounts store a lot of our information. Exposure of this information by people with bad intentions can compromise our well-being. It is important to protect this information, and one of the most important aspects to protect it is the proper use of passwords. Block access to your mobile devices by locking the screen with a password, and use long passwords for your online accounts, unique for each account, with numbers, letters and characters, and change them periodically. We also recommend using a password manager such as Bitwarden to store them and not forget them.

</div>
