---
numeroHistoria: Story 5
titulo: Protecting Collective Spaces Online
imagen: "/imagenes/historias/historia-mid-ilustracion.png"
tags:
  - activistas_ingles
---

Noah is a LGBTQI+ activist from the association Distintxs and manages the social networks of the group of activists in their community as a volunteer. Noah is a non-binary person who lives with her family. Although they get along well with most members of his household, they have a rocky relationship with their older brother, who does not accept his gender identity.

One morning, Noah wakes up to their phone overflowing with messages from fellow members of the activist group. The group's Facebook page had been hacked, none of them had access to it, and virtually all the content they had posted over the years had been deleted. After several attempts to recover the original page, they eventually gave up and started a new page, never understanding what had happened.

Months later, during a family fight, Noah's brother confesses to breaking into Noah's personal computer, where he found their Facebook profile and email open, and used Noah's administrator permissions and email to take control of the page and delete the content.

<div class="apartado">

## How could the collective have prevented this situation?

It is important to note that, although the attack came from Noah's brother, it is not their fault, as the care of the page is a collective responsibility. The implementation of a proper password policy for Distintxs is fundamental for the protection of the organization's assets. It is also important for members to generate spaces for discussion where they can identify possible threats to their activism work, the possible consequences of an attack, and how to prevent future incidents.

</div>
