---
numeroHistoria: Story 4
titulo: Isaura against the police
imagen: "/imagenes/historias/historia-top-ilustracion.png"
tags:
  - activistas_ingles
---

Isaura is a feminist activist and LGBTQI+ living in Hernandarias. At the last Pride march held in her town, she and a group of friends witnessed a group of trans women being mistreated by the police. Isaura used her cell phone to record the aggression, but an officer saw her and quickly snatched the phone, which was fingerprint-protected.

Through the use of force, the officer got Isaura to place her finger on the fingerprint reader, thus being able to access the device and remove the evidence. Although Isaura attempted to report the assault, she was not heard for lack of evidence.

<div class="apartado">

## What steps could Isaura have taken to protect her device??

It is important to keep in mind that when we participate in marches, demonstrations or protests, we should use strategies that help us take care of both our physical and digital bodies. TEDIC's [Digital Security Toolkit for Collective Actions](https://www.tedic.org/wp-content/uploads/2020/01/Toolkit-TEDIC_Formato-Digital.pdf) has a series of useful tools to implement before, during and after participating in a demonstration.

In the case of Isaura, the use of device blocking through biometric data, such as face or finger, can bring several security issues. The fingerprint is relatively easy to obtain, either through force or other means (for example, while the owner of the phone is sleeping).

</div>
