---
numeroHistoria: Story 7
titulo: Liliana and Jorge are blackmailed
imagen: "/imagenes/historias/historia-top-ilustracion.png"
tags:
  - figuras_ingles
---

Liliana is a well-known woman in the national political scene. Due to the conservative climate in her country, she decided years ago to hide her sexual identity as a lesbian. Liliana is also married to Jorge, a famous TV host who is also not out. Both Jorge and Liliana use dating apps to live their sex lives to the fullest. One day, she and Jorge receive an email with intimate photographs of him and homoerotic online sex conversations, and the threat of publishing Jorge's activity, which could ruin both of their careers. In exchange for silence, the extortionist asked for a large sum of money.

<div class="apartado">

## How could Liliana and Jorge have avoided this situation?

Sexting is risk-free by itself and is part of our sexual and reproductive rights, but in practice it can become problematic, giving rise to many complicated situations: violation of our rights to privacy or freedom of expression, violation of our sexual rights through extortion or distribution of intimate images without consent, as well as the publication of personal data without authorization.

TEDIC recommends using applications that protect our privacy when sexting. Signal is a messenger that has privacy by design, that is, it was created with the user's privacy in mind from the beginning. Features such as periodic deletion of texts, face blurring, and end-to-end encryption make this tool an ally when it comes to expressing our sexual desire using digital media.

</div>
