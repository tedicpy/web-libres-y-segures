---
numeroHistoria: Sotry 1
titulo: Carlos v. social networks
Adversary: gossiping neighbors
imagen: "/imagenes/historias/historia-top-ilustracion.png"
tags:
  - queer_ingles
---

Carlos is a very friendly person and likes to have friends on social networks. They are well liked and known by everyone in their work, university and neighborhood. Carlos also likes to go out dancing a lot. One of their friends uploaded pictures of them at the last drag party at Hollywood, the famous gay disco in Asuncion, and tagged them on Facebook. Although Carlos made sure to hide the posts on their feed from their family members (because they didn't know about his sexual orientation yet), a neighbor of their saw the photos and it wasn't long before it was gossip at the neighborhood hairdresser. Carlos’ family found out and now is staying at a friend's house to avoid confrontation.


<div class="apartado">

## How could Carlos have avoided this situation?

It is important for each of us to identify what information we prefer to have on a public profile and what information we prefer to have on a private profile. The privacy options on Facebook allow us to take control over who sees what kind of information on our public profiles, and also allow us to control who can associate information with our identity (for example, choose whether we want to be tagged in photos or not).

</div>
