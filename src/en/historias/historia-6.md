---
numeroHistoria: Story 6
titulo: Pandemic Zoombombing
imagen: "/imagenes/historias/historia-bot-ilustracion.png"
tags:
  - activistas_ingles
---

José is part of an organization that has been working for the defense of the rights of the LGBTQI+ community for several years. His organization holds meetings, talks and debates about the issues of the community. Although these meetings have always been face-to-face, the pandemic situation made it necessary to start holding virtual events, trying to keep communal links in times of physical isolation.

So far, the virtual meetings have not had good results. The first meeting organized by José was the scene of a case of Zoombombing, where anti-rights strangers entered the chat and insulted the participants. These situations, which reoccur from time to time, have been quite discouraging to José and his community.

<div class="apartado">

## How can José help to improve this situation?

The use of appropriate communication and videoconferencing tools is essential to avoid such incidents. Using free tools such as Jitsi to conduct these meetings and sending the access links through secure means of communication, such as Signal, can help prevent the intrusion of unwanted people into these types of meetings.

</div>
