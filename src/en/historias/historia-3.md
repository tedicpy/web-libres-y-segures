---
numeroHistoria: Historia 3
titulo: Mercedes was scammed
Adversario: Scammers
imagen: "/imagenes/historias/historia-bot-ilustracion.png"
tags:
  - queer_ingles
---

Mercedes came out recently and is ready to start dating. Although she had the opportunity to go a couple of times to queer spaces to meet other women with her same interests, she feels more comfortable using dating apps, especially because of the pandemic situation. One day she met Fabiana and they quickly became very close. Although Fabiana did not like to share personal photos, make calls or send audios, they texted each other every day, and grew a very intimate relationship.

One day, Fabiana texted Mercedes, very upset, and told her that her mother had been admitted to the ICU the day before. She had a very serious case of COVID-19, and her family was desperate, as they did not have enough money to cover the medical expenses. Moved by Fabiana's situation, Mercedes offered to help her by sending a large sum of money through Giros Tigo as a loan. Once this transaction was completed, Mercedes noticed that she has been blocked by Fabiana on both WhatsApp and the dating app where they met. After trying to communicate with Fabiana through different means, she finally accepts that she was the victim of a scam.

<div class="apartado">

## How could Mercedes have avoided this situation?

The Rohendu 2020 report conducted by Aireana, a group for the defense of lesbian rights in Paraguay, states that many LGBTQI+ young people reported to the Rohendu helpline to be victims of theft, scams or even aggression by users of social networks and dating apps.

Mercedes' case is an example of catfishing, a type of scam where the perpetrator creates a fake profile and uses it to generate a close relationship with a victim, with the aim of scamming or abusing a targeted person. This activity can be used for financial gain, to jeopardize the victim, or to generate distress. Catfishing is a type of phishing, an umbrella term for various online activities aimed at extracting information or defrauding a victim. 

</div>
